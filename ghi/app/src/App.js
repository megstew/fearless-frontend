import Nav from './nav';
import AttendConferenceForm from './AttendConferenceForm';
// import ConferenceForm from './ConferenceForm';
// import AttendeesList from './AttendeesList';
// import LocationForm from './LocationForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
      <AttendConferenceForm />
      {/* <ConferenceForm/> */}
    {/* <LocationForm /> */}
    {/* <AttendeesList attendees={props.attendees}/> */}
    </div>
    </>
  );
}

export default App;
