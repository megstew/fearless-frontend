function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">Start Date: ${startDate}<br>End Date: ${endDate}</div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.error('An ERROR occurred.');
        } else {
            const data = await response.json();

            const columns = document.querySelectorAll('.col');
            let columnIndex = 0;

            for (let conference of data.conferences) {

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const name = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const startDate = new Date(details.conference.starts).toLocaleDateString();
                  const endDate = new Date(details.conference.ends).toLocaleDateString();
                  const location = details.conference.location.name;
                  const html = createCard(name, description, pictureUrl, startDate, endDate, location);
                  const column = columns[columnIndex];
                  column.innerHTML += html;
                  columnIndex = (columnIndex + 1) % 3;
                }
              }
    }
    } catch (e) {
        console.error(e);
        const htmlError = `
        <div class="alert alert-danger" role="alert">
        An Error Occured While Fetching Conference Data ${e.message}
        </div>
        `;
        const column = querySelector('.col');
        column.innerHTML += htmlError;
    }
});
